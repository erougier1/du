def recherche_pt_fixe(a) :
    inf, sup = 0, len(a) - 1
    while sup > inf:
        milieu = (inf + sup) // 2
        if a[milieu] == milieu :
            return milieu
        if a[milieu] > milieu :
            sup = milieu
            print('sup' + str(sup))
        else :
            inf = milieu
            print('inf' + str(inf))
    if a[sup] == sup :
        return sup
    raise ValueError("Pas de point fixe")            
    
def rechercheMax(l):
    ### Recherche dichotomique du max de l, liste sans répétition en dents de scie###
    i=int(len(l)/2)
    bsup=len(l)
    binf=0
    trouve=False
    while not trouve and bsup-binf>1:
        if i==0 or i==len(l):
            trouve = True
        elif l[i]<=l[i+1]:
            binf=i
        else:
            bsup=i
        i = int((binf+bsup)/2)
    if l[bsup]>l[binf]:
        return (bsup,l[bsup])
    if l[bsup]<l[binf]:
        return (binf,l[binf]) 



def russky_moujik(x,y) :
    def moujikchou(a,b,acc) :
        if a == 0 : 
            return acc
        return moujikchou(a//2, b*2, acc + b*(a % 2))
    return moujikchou(x, y, 0)
    
def russian_peasant(x,y) :
    a, b, acc = x, y, 0
    while a > 0 :
        acc += b*(a % 2)
        b *= 2
        a //= 2
        print(a*b + acc)
    return acc
