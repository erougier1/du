module Liste where

infix |-

data Liste a = Vide
             | a |- (Liste a)
               deriving (Show, Eq)
             
longueur :: Liste a -> Int
longueur Vide     = 0
longueur (t |- q)  = 1 + (longueur q)
                  
applique :: (a -> a) -> Liste a -> Liste a
applique f Vide = Vide
applique f (t |- q)  = (f t) |- (applique f q) 
