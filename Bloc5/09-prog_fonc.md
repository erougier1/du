# Programmation fonctionnelle



## Une programmation utilisée

On  pourra compléter  sa  lecture par  l'étude  de la  [section  sur les  autres
langages de programmation](https://gitlab.com/GiYoM/du/blob/master/Bloc5/12-d_autres_langages.md)

Au moment d'être racheté en 2014 par Facebook, WhatsApp comptait 32 informaticiens pour
gérer 1 milliard  d'utilisateurs par mois !  Le secret ? Whatsapp  est écrit en
`Erlang`, langage concurrent et fonctionnel.

Facebook a recruté de nombreux programmeurs (français) `Ocaml` et utilise aussi des outils développés en `Haskell` (`haxl`)

Twitter a commencé en `Ruby` mais en grandissant, Twitter est passé à `Scala` pour son aspect fonctionnel.

De nombreuses banques (Deutsche Bank, Barclays, Crédit Suisse, BNP, JPMorgan...) utilisent Haskell.

Google utilise `Haskell`

`Kotlin` est le langage officiel d'Android  depuis mai dernier. Il est largement
fonctionnel.

`Rust` est le langage phare de Mozilla et est largement fonctionnel.



Le langage `Go` lancé par Google a beaucoup d'aspects fonctionnels comme le typage statique fondé sur l'inférence de type, les fonctions d'ordre supérieur, l'évaluation paresseuse.

Le New York Times pour gérer les grands flux d'information


De manière générale, le style fonctionnel est de plus en plus influent depuis quelques années.

Dans les 10 langages associés aux salaires les plus élevés aux États-Unis, 7 ont
de  fortes connotations  fonctionnelles  : `Scala`,  `Clojure`,  `Erlang`,
`Kotlin`, `Rust`, `F#`, `Elixir` et on pourrait même ajouter `Go`. Voir la [page stackoverflow](https://insights.stackoverflow.com/survey/2019#technology-_-what-languages-are-associated-with-the-highest-salaries-worldwide)



## Une programmation déclarative paresseuse

**Quelle est la somme des 100 premiers entiers naturels pairs tels que le carré de la
somme d'un de ces nombres et de son successeur est
divisible par 13793 ?**

Première idée avec Python, mais...

```python
a = 0
b = 0
c = 0
while b < 100:
	if (c + c + 1)**2 % 13793 == 0:
		a += c
		b +=1
	c += 2
print(a)
```

Avec Haskell :

```haskell
sum $ take 100 $ filter (\x -> (x + x + 1)^2 `mod` 13793 == 0) [0,2..]
```

Avec Python influencé par Haskell, mais...

```python
gen = (x for x in range(10000000000000000) if (x + x + 1)**2 % 13793 == 0 and x % 2 == 0)
sum([next(gen) for _ in range(100)])
```

ou en utilisant la fonction `count` de la bibliothèque `itertools`, mais...

```python
gen = (x for x in __import__("itertools").count(0,2) if (x + x + 1)**2 % 13793 == 0)
sum([next(gen) for _ in range(100)])
```

## Transparence référencielle

```python
n: int = 1

def inc(k: int) -> int:
    global n
    n = n + k
    return n


for appel in range(1,5):
    print(f"\n Appel numéro  {appel} : inc(1) + inc(2) = {inc(1) + inc(2)}")
```

```console
 Appel numéro  1 : inc(1) + inc(2) = 6

 Appel numéro  2 : inc(1) + inc(2) = 12

 Appel numéro  3 : inc(1) + inc(2) = 18

 Appel numéro  4 : inc(1) + inc(2) = 24
```

Ça peut  être pratique  pour moins  se casser  la tête  au moment  de programmer
mais...



>Insanity Is  Doing the Same Thing  Over and Over Again  and Expecting Different
>Results

Avoir de la transparence référentielle offre de nombreux avantages :

* **modularisation** : pas de notion  d'état des variables donc on peut découper
  en  petits   modules  selon  l'architecture  "botom-up".   Cela  simplifie  la
  maintenance, permet la  réutilisation. On construit les briques du  lego et on
  les assemble.
  
* **idempotence** : vous obtenez toujours  le même résultat pour chaque appel de
  la fonction.
  
* **parallélisation** : du fait de  l'indépendance des appels d'une fonction, on
  peut les effectuer indépendemment. Par exemple si 
  `res = f1(a,b) + f2(a,c)`, on peut  paralléliser les appels à `f1` et `f2` car
  `a` ne sera pas modifié.
  
* **mémoïsation** :  on peut garder en  cache les appels à une  fonction dans un
  cache. Cela facilite la programmation dynamique. Voici une version *Top-Down*

En Haskell:

```haskell
fib :: Int -> Integer
fib = (map fib' [0..] !!)
	    where fib' 0 = 0
		      fib' 1 = 1
		      fib' n = fib (n-2) + fib (n-1)
```

En Python

```python
A = TypeVar('A')
B = TypeVar('B')

def memoise(f: Callable[[A], B]) -> Callable[[A], B]:
    cache: Dict[A, B] = {}
    def f_avec_cache(n: A) -> B:
        if n not in cache:
            cache[n] = f(n)
        return cache[n]
    return f_avec_cache

fib: Callable[[int], int] = lambda n: 0 if n==0 else 1 if n==1 else fib(n-2) + fib(n-1)

fib = memoise(fib)
```

Avec décorateur:

```python
A = TypeVar('A')
B = TypeVar('B')

def memoise(f: Callable[[A], B]) -> Callable[[A], B]:
    cache: Dict[A, B] = {}
    def f_avec_cache(n):
        if n not in cache:
            cache[n] = f(n)
        return cache[n]
    return f_avec_cache

@memoise
def fib(n: int) -> int:
    return 0 if n==0 else 1 if n==1 else fib(n-2) + fib(n-1)
```

Avec un décorateur et une classe idoine :

```python
class Memoise:

    def __init__(self, f):
        self.f = f
        self.cache = {}
        
    def __call__(self, *args) -> B:
        if args not in self.cache:
            self.cache[args] = self.f(*args)
        return self.cache[args]


@Memoise
def Fib(n: int) -> int:
    if n in {0,1}:
        return n
    return Fib(n-2) + Fib(n-1)
```






## Récursion ne signifie pas Pile

En Haskell :

```haskell
fib :: Int -> Integer
fib = \n -> fib' n 0 1
       where fib' 0 prems _    = prems
             fib' n prems deuz = fib' (n-1) deuz (prems + deuz)
```

En JavaScript :

```javascript
function fib(n) {
	function fib_aux(n, prems, deuz) {
		if (n > 0) {
			return fib_aux(n - 1, deuz, prems + deuz);
			}
		else {
			return prems;
			} 
	}
	return fib_aux(n, 0, 1);
}
```

En PHP :

```php
function fib($n, $prems = 0, $deuz = 1) 
{ 
    if ($n == 0) 
        return $a; 
    return fib($n - 1, $deuz, $prems + $deuz); 
} 
```


En Python, la possibilité  de donner des arguments par défaut  nous permet de ne
pas avoir à créer une fonction imbriquée :

```python
def fib(n: int, prems:int = 0, deuz:int = 1) -> int:
    if n == 0:
        return prems
    return fib(n-1, deuz, prems + deuz)
```


## Pas d'affectation : des liens <3

ou encore : **ne modifiez pas ! Créez !**

Au lieu de :

```python
nom = 'James'
nom = nom + 'Bond'
```

Préférez :

```python
prenom = 'James'
nom = 'Bond'
appellation = prenom + nom
```

## Fonctions d'ordre supérieur

Une fonction est un objet comme un autre :)

```python
def appeler(f:Callable[[str], str]) -> str:
	return f('James Bond')
	
def anglais(name: str) -> str:
	return f'Hello, my name is {name}'
	
def francais(nom: str) -> str:
	return f'Bonjour, mon nom est {nom}'
```

`appeler`,  `anglais` et 'francais' sont des fonctions et pourtant :

```python
In [1]: appeler(anglais)
Out[1]: 'Hello, my name is James Bond'

In [2]: appeler(francais)
Out[2]: 'Bonjour, mon nom est James Bond'
```


## Construire des types algébriques

Construisons **en Haskell** rapidement un type pour représenter les arbres binaires de recherche:

```haskell
module ArbreBinaire where

data Arbre a = ArbreVide
            | Noeud a (Arbre a) (Arbre a) deriving (Read, Eq, Show)


insere :: (Ord a) => a -> Arbre a -> Arbre a
insere x ArbreVide = Noeud x ArbreVide ArbreVide
insere x (Noeud a gauche droite)
    | x == a = Noeud x gauche droite
    | x <  a = Noeud a (insere x gauche) droite
    | x >  a = Noeud a gauche (insere x droite)

contient :: (Ord a) => a -> Arbre a -> Bool
contient x ArbreVide = False
contient x (Noeud a gauche droite)
    | x == a = True
    | x <  a = contient x gauche
    | x >  a = contient x droite

hauteur :: Arbre a -> Int
hauteur ArbreVide = 0
hauteur (Noeud a gauche droite) = maximum [1, lh, rh]
    where lh = 1 + hauteur gauche
          rh = 1 + hauteur droite

makeArbre :: (Ord a) => [a] -> Arbre a
makeArbre = foldr insere ArbreVide . reverse

```

C'est quand même plus simple....


## Monades


Les monades sont dans l'esprit de nombreux informaticiens un outil mystérieux et
incompréhensible réservés aux adeptes de Haskell. 

Pourtant Microsoft  a lancé  en 2002  son shell  qui a  tout d'abord  été appelé
`Monad`  (mais  a   été  renommé  `PowerShell`  pour  ne  pas   faire  peur  aux
développeurs). 

On  peut  penser aux  monades  comme  à un  monde  situé  de l'autre  côté  d'un
tunnel. On  ne peut y accéder  mais on peut  envoyer des choses y  travailler et
récupérer leur travail.

Prenons l'exemple de `Maybe` en Haskell (mais le principe est 'exportable' !).


![context](./IMG/21_context.png)



```haskell
demi :: Integral a => a -> Maybe a
demi x = if even x
           then Just (x `div` 2)
           else Nothing  
```

![just](./IMG/22_half.png)

```haskell
λ> demi 4
Just 2
λ> demi 3
Nothing
```



![fmap](./IMG/09_fmap_just.png)


```haskell
λ> fmap (+3) (Just 4)
Just 7
```


![fmap_def](./IMG/08_fmap_def.png)

```haskell
halve :: Int -> Maybe Int
halve n = guard (even n) >> return (n `div` 2)
```


```haskell
getLine >>= readFile >>= putStrLn 
```


![ps](./IMG/powershell.png)


On  réfléchit à  un OS  "monadique" où  les différents  langages échangent  sans
passer par la case texte.


![mx](./IMG/multix.png)

