# De nouveaux langages

Nous avons déjà parlé de Scala (2003) mais depuis de beaux langages sont apparus
ayant en commun la volonté d'améliorer ce qui existe, le rendre plus sûr.

## Rust

Le premier  noyau stable  a été  publié en  2015. C'est  un langage  soutenu par
Mozilla. Son nom traduit le fait que Rust s'appuie sur des concepts qui ont fait
leurs preuves mais qui ne sont pas forcément réunis dans un même langage. Rust a
de nombreux concepts tirés du monde fonctionnel:

* typage statique
* variables immuables
* inférence de type
* filtrage par motif
* généricité

Il introduit un concept original d'appartenance et d'emprunt.

```rust
fn main() {
   let x = 1;
   x = 2; 
   let v = vec![1, 2, 3];
   let w = v;
   println!("{}", v[0]);
}
```

On lance la compilation et voici le résultat :

```rust

error[E0384]: cannot assign twice to immutable variable `x`
 --> ./UCO/DU/Bloc5/rust.rs:3:4
  |
2 |    let x = 1;
  |        -
  |        |
  |        first assignment to `x`
  |        help: make this binding mutable: `mut x`
3 |    x = 2;    
  |    ^^^^^ cannot assign twice to immutable variable

error[E0382]: borrow of moved value: `v`
 --> ./UCO/DU/Bloc5/rust.rs:6:19
  |
4 |    let v = vec![1, 2, 3];
  |        - move occurs because `v` has type `std::vec::Vec<i32>`, which does not implement the `Copy` trait
5 |    let w = v;
  |            - value moved here
6 |    println!("{}", v[0]);
  |                   ^ value borrowed here after move

error: aborting due to 2 previous errors

Some errors have detailed explanations: E0382, E0384.
For more information about an error, try `rustc --explain E0382`.
```



## Crystal


Crystal est un langage encore plus récent, inspiré de Ruby, où tout est objet mais
où le  typage est  statique et  permet une  inférence de  type. La  syntaxe veut
éviter le  *boilerplate code* et  apparaît comme  plus sympathique que  celle de
Python. 

```crystal
class Animal
end

class Dog < Animal
  def talk
    "Woof!"
  end
end

class Cat < Animal
  def talk
    "Miau"
  end
end

class Person
  getter pet

  def initialize(@name : String, @pet : Animal)
  end
end

john = Person.new "John", Dog.new
peter = Person.new "Peter", Cat.new


john.pet.talk #=> "Woof!"
```


## Elixir

Regardons la doc

*Elixir is a dynamic, functional language designed for building scalable and maintainable applications.
Elixir leverages the  Erlang VM, known for running  low-latency, distributed and
fault-tolerant systems,  while also being  successfully used in  web development
and the embedded software domain.*


```elixir
current_process = self()

# Spawn an Elixir process (not an operating system one!)
spawn_link(fn ->
  send(current_process, {:msg, "hello world"})
end)

# Block until the message is received
receive do
  {:msg, contents} -> IO.puts(contents)
end
```


Fault tolerance

```elixir
children = [
  TCP.Pool,
  {TCP.Acceptor, port: 4040}
]

Supervisor.start_link(children, strategy: :one_for_one)
```

Functionnal programming:

```elixir
def drive(%User{age: age}) when age >= 16 do
  # Code that drives a car
end

drive(User.get("John Doe"))
#=> Fails if the user is under 16
```




## Kotlin

 Kotlin est devenu  officiellement le 9 mai dernier le  langage de programmation
 voulu et  recommandé par le  géant américain  Google pour le  développement des
 applications Android !
 
 
Inspiré de Ruby qui lui donne son aspect objet, il est aussi très fonctionnel:

```kotlin

// data class with parameters and their optional default values

data class Book(val name: String = "", val price: Int = 0)


fun main(args: Array) {

    // create a data class object like any other class object

    var book1 = Book("Kotlin Programming",250)

    println(book1)

    // output: Book(name=Kotlin Programming, price=250)

}
```
 
 
 ```kotlin
 // the following function takes a lambda, f, and executes f passing it the string, "lambda"
// note that (s: String) -> Unit indicates a lambda with a String parameter and Unit return type
fun executeLambda(f: (s: String) -> Unit) {
    f("lambda")
}
 ```
 
 
```kotlin
// the following statement defines a lambda that takes a single parameter and passes it to the println function
val l = { c : Any? -> println(c) }
// lambdas with no parameters may simply be defined using { }
val l2 = { print("no parameters") }
```



## Go

Développé  pour  Google  par  des  stars (Robert  Griesemer,  Rob  Pike  et  Ken
Thompson), c'est un langage au typage fort, statique et structurel.

D'après Wikipedia:
*S'il  vise aussi  la  rapidité d'exécution,  indispensable  à la  programmation
système, il considère le multithreading comme le moyen le plus robuste d'assurer
sur  les processeurs  actuels cette  rapidité8  tout en  rendant la  maintenance
facile par séparation  de tâches simples exécutées  indépendamment afin d'éviter
de  créer  des  «  usines  à   gaz  ».  Cette  conception  permet  également  le
fonctionnement sans  réécriture sur des architectures  multi-cœurs en exploitant
immédiatement l'augmentation de puissance correspondante.*

Mais il est 1h10 et je suis fatigué...
