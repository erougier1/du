fic = open('vote_senateurs.txt')
# ficher brut

brut2str = fic.read()
# conversion en une chaîne de caractères

str2liste = [ ligne.split(' ') for ligne in brut2str.split('\n') ]
# liste de listes de type ['nom', 'état', 'parti', '1', '1', '-1', etc. ]

origine = { data_senateurs[0] : data_senateurs[1:3] for data_senateurs in str2liste }
# dictionnaire clé : nom du sénateur, valeur = liste ['parti', 'état']

votes = { data_senateurs[0] : [int(vote) for vote in data_senateurs[3:]] for data_senateurs in str2liste }
# dictionnaire clé : nom du sénateur, valeur = liste des votes [1,1,-1,1,...]

def etat(sen) :
    return origine[sen][1]
    
def parti(sen) :
    return origine[sen][0]
    
les_senateurs = {sen for sen in origine}
        
    
def prod_scal(xs, ys) :
    return sum([xs[i] * ys[i] for i in range(min(len(xs), len(ys)))])
    
def coherence(sen1, sen2) :
    return prod_scal(votes[sen1], votes[sen2])
    
def coherence_etat(state) :
    paire = {sen for sen in les_senateurs if etat(sen) == state}
    return ( coherence(*paire) if len(paire) == 2 else 46, state, paire )


    



 
