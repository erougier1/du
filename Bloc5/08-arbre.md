# Arbres binaires de recherche

## Le projet

Nous  voudrions  savoir si  un  certain  mot apparaît  dans  "*Du  côté de  chez
Swann*". Il y a 15731 mots différents.  Si nous n'avons pas de chance, cela nous
demandera  15730 comparaisons.  Pouvons-nous faire  mieux? Nous  voudrions aussi
avoir l'ensemble des mots dans l'ordre  alphabétique, le premier mot, le dernier
mot dans cet ordre.

## Les thèmes

Outre la notion d'ABR, nous allons aborder les thèmes suivants:

* création de classes;
* typage
* interface/implémentation
* structures de données
* récursivité
* complexité
* modularité
* POO/Programmation fonctionnelle
* tri
* lecture/écriture de fichiers
* manipulations de textes



## Arbre binaire de recherche


Dans un tel arbre, chaque noeud a  une valeur et pointe vers deux noeuds enfants
éventuellement  vides. La  valeur  de  tout noeud  du  sous-arbre inférieur  est
inférieure à la valeur du noeud. Idem pour le sous-arbre supérieur.

![arbre binaire](./IMG/arbre1.png)


## Le but du jeu

Nous voudrions analyser le  texte de "[Du côté de chez  Swann](https://www.gutenberg.org/files/2650/2650-0.txt)" qui est disponible
sur le site de Gutenberg: 

```python
from urllib.request import urlretrieve

urlretrieve('https://www.gutenberg.org/files/2650/2650-0.txt', 'swann.txt')
fic = open('swann.txt', 'r').read()
```

`fic` est une chaîne de caractère:

```python
In [4]: type(fic)
Out[4]: str

In [5]: type(open('swann.txt', 'r'))
Out[5]: _io.TextIOWrapper
```

Nous allons  donner un exemple  de post-traitement de  texte. En effet,  nous ne
voulons   retenir  que   les  mots,   pas  les   chiffres  ni   les  signes   de
ponctuation.  Nous   allons  utiliser  la  bibliothèque   `re`  (comme  *Regular
Expression*) en donnant comme consigne de remplacer tout caractère qui n'est pas
une lettre par une espace:

```python
texte = re.sub('[^A-ZÉÈÀÙÊÔÂÜÛÏÎa-zéèàùêôâüûïî]+', ' ', fic)
```

Puis  on construit  l'**ensemble** des  mots qu'on  prend le  soin de  mettre en
minuscules:

```python
mots = {m.lower() for m in texte.split()}
```

On va ensuite insérer ces mots dans un ABR:

```python
sw: Arbre[str] = Arbre()
for mot in mots:
    sw.insere(mot)
```

Et voici quelques renseignements sur le texte:

```python
In [8]: sw.hauteur()
Out[11]: 33

In [12]: sw.maxi()
Out[17]: 'ût'

In [18]: sw.mini()
Out[18]: 'a'

In [19]: sw.nb_feuilles()
Out[19]: 5209

In [20]: sw.nb_noeuds()
Out[22]: 15731

In [23]: sw.contient('madeleine')
En 18 comparaisons
Out[25]: True

In [26]: sw.contient('binaire')
En 25 comparaisons
Out[26]: False

In [27]: sw.visite_inf()
a
abaissa
abaissait
abaissant
abaisse
abaisser
abaissée
abaissés
abandon
abandonnai
abandonnait
abandonné
abandonnée
abandonnées
abandonnés
abasourdi
abat
...
```


## Typage structurel : Protocol

On voudrait pouvoir  s'assurer que notre classe ne permette  de créer des arbres
qu'avec des types d'objets "comparables". Il faut donc s'assurer que la classe des
objets  contenus dans  l'arbre  admet  les méthodes  usuelles  de comparaison  :
`__eq__`, `__lt__`, `__gt__`.

Pour cela on va utiliser `Protocol` 
(cf [PEP 544](https://www.python.org/dev/peps/pep-0544/))

L'outil         de        vérification         de        typage         statique
[`mypy`](https://mypy.readthedocs.io/en/stable/index.html)  permettra  alors  de
s'assurer de la cohérence structurelle du type des valeurs des noeuds.


Pour cela, on crée une classe **abstraite** d'objets comparables:


```python
from typing_extensions import Protocol # Python 3.7
from abc import abstractmethod

class Comparable(Protocol):
    """
    Un protocole est un type abstrait dont les sous-types ont la même
    structure.
    Ici, on crée un type d'objets comparables : les objets doivent
    pouvoir être comparés avec les méthodes ==, < et >
    """
    @abstractmethod
    def __eq__(self, other: Any) -> bool:
        pass

    @abstractmethod
    def __lt__(self: C, other: C) -> bool:
        pass

    def __gt__(self: C, other: C) -> bool:
        return (not self < other) and self != other

```

Puis on crée un type, nommé `C` par exemple, qui est lié à cette classe:

```python
from typing import TypeVar

C = TypeVar("C", bound="Comparable")
```



## Type générique

On veut ensuite s'assurer que tous les noeuds de l'arbre contiennent des noeuds de
même type.  On peut  alors créer une  classe `Noeud` qui  dépend d'un  seul type
d'objets. On utilise alors la notion de *type générique* 
(cf [PEP 484](https://www.python.org/dev/peps/pep-0484/#generics)).


```python
from typing import Optional, Generic

class Noeud(Generic[C]):
    """
    Un noeud a une valeur et pointe vers deux autres noeuds (petit et
    grand) ou éventuellement le vide.
    On insère de nouvelles valeurs en partant de la racine du noeud et
    en bifurquant selon la comparaison avec la valeur du noeud.

    """

    def __init__(self: 'Noeud[C]', val: C) -> None:
        """
        Un noeud a toujours une valeur mais pointe vers un autre noeud
        ou éventuellement le vide (None)
        """
        self.__val: C = val
        self.__grand: Optional['Noeud[C]'] = None
        self.__petit: Optional['Noeud[C]'] = None
```


Sachant qu'une branche de noeud peut  être vide ou  contenir une valeur,  le type  des deux branches du noeud est `Optional['Noeud[C]']`:  c'est un objet de type `Noeud[C]`
ou `NoneType` s'il est vide.

Notez les `__` qui précèdent la définition  des attributs : cela rend leur usage
*privé*  i.e.  limité  à un  usage  interne  à  la  classe pour  construire  les
différents méthodes.




## Insertion

Nous  sommes alors  prêts  à créer  notre première  méthode  qui doit  permettre
d'insérer un élément au bon endroit. Il est naturel avec un ABR de créer une
méthode récursive:


```python
    def insere(self: 'Noeud[C]', val: C) -> None:
        """
        Permet d'insérer une valeur comparable dans un noeud.
        """
        if val < self.__val:
            if self.__petit:
                self.__petit.insere(val)
            else:
                self.__petit = Noeud(val)
        elif val > self.__val:
            if self.__grand:
                self.__grand.insere(val)
            else:
                self.__grand = Noeud(val)
        else:
            print(f'{val} est un doublon')

```


## Modularité

Il y a de nombreuses façons d'implémenter les noeuds. Mais, quelle qu'elle soit,
on pourra construire nos arbres à partir de la même interface:


```python
from nbr import * # le fichier contenant la classe Noeud est nommé nbr.py

class Arbre(Generic[C]):
    """
    Arbre binaire de recherche constitué de noeuds.
    Reprend les méthodes de la classe Noeud en incluant le cas vide
    et en construisant un arbre à partir d'un noeud.
    """

    def __init__(self: 'Arbre[C]') -> None:
        """
        Constructeur : un arbre est vide ou constitué de noeuds
        """
        self.__data: Optional[Noeud[C]] = None

    def insere(self: 'Arbre[C]', val: C) -> None:
        """
        Insère un élément comparable dans un arbre selon le critère
        choisi pour les noeuds.
        Si l'arbre est vide, crée le noeud-data
        """
        if self.__data is None:
            self.__data = Noeud(val)
        else:
            self.__data.insere(val)

```




### NB : programmation fonctionnelle

En programmation fonctionnelle, tout ceci peut se résumer à (ici en Haskell):


```haskell
data Arbre a = ArbreVide 
	| Noeud a (Arbre a) (Arbre a) 
	deriving (Show, Read, Eq) 

feuille :: a -> Arbre a  
feuille x = Noeud x ArbreVide ArbreVide  

insere :: (Ord c) => c -> Arbre c -> Arbre c  
insere x ArbreVide = feuille x  
insere x (Noeud n petit grand)   
	| x == n = Noeud x petit grand  
	| x < n  = Noeud n (insere x petit) grand  
	| x > n  = Noeud n petit (insere x grand)

```


## Les méthodes de base

À vous de jouer: créez les méthodes suivantes:

```python
    def hauteur(self: 'Noeud[C]') -> int:
        """
        Nombre de niveaux de l'arbre
        """
	def nb_noeuds(self: 'Noeud[C]') -> int:
	
	def est_feuille(self: 'Noeud[C]') -> bool:
	
	def est_unaire(self: 'Noeud[C]') -> bool:
	
	def nb_feuilles(self: 'Noeud[C]') -> int:
	
	def mini(self: 'Noeud[C]') -> C:

    def maxi(self: 'Noeud[C]') -> C:
	
	def contient(self: 'Noeud[C]', v: C, cpt=0) -> bool:
```


## Parcours en profondeur/largeur

Nous voudrions afficher les valeurs des noeuds de l'arbre.


![arbre2](./IMG/arbre2.png)


Voici 4 façons d'énumérer ces valeurs:

```python
In [5]: a.visite_pre()
495 356 72  244  73 344 303 421 
441 488 997 661 637 555 744 870

In [11]: a.visite_post()
 73 303 344 244  72 488 441 421
356 555 637 870 744 661 997 495

In [12]: a.visite_inf()
 72  73 244 303 344 356 421 441
488 495 555 637 661 744 870 997

In [13]: a.visite_niveau()
495 356 997  72 421 661 244 441
637 744  73 344 488 555 870 303
```

Les 3 premières ont une même particularité qui les distingue de la dernière. Laquelle ?

Faites un parcours en profondeur en dessinant le contour de l'arbre. 
* si vous notez la valeur du noeud en passant à gauche des noeuds, vous faites une
  visite prefixe;
* si vous la notez en passant sous le noeud, c'est un parcours infixe;
* si vous la notez en passant à droite du noeud, c'est un parcours postfixe.






Quel est l'intérêt de `visite_inf` ?

En voici une implémentation:

```python
    def visite_inf(self: 'Noeud[C]') -> None:
        if self.__petit:
            self.__petit.visite_inf()
        print(self.__val)
        if self.__grand:
            self.__grand.visite_inf()
```

Quelle est sa complexité ?

*Donnez-en une version non récursive en utilisant par exemple une pile.*


*Donner une version non récursive de `visite_niveau`*

## Affichage avec Graphviz


[Graphviz](https://graphviz.org/)  est   un  logiciel  largement   utilisé  pour
visualiser des graphes et très simple d'utilisation.

Chaque arête est représentée sous la forme `père->fils;`

On ajoute des petites couleurs pour faire bien:

```python
   def tree2viz(self: 'Noeud[C]') -> str:
        """
        Utilise graphviz pour représenter l'arbre.
        Au besoin, l'installer:
          $ sudo apt-get install graphviz
        Chaque arête est représentée sous la forme:
        père -> fils;
        """
        s = ''
        v = self.__val
        if self.__petit and self.__grand:
            s += f'{self.__petit.__val}[style=filled,fillcolor=cadetblue3];\n'
            s += f'{self.__grand.__val}[style=filled,fillcolor=darkorange];\n'
            s += f'{v}->{self.__petit.__val};\n'
            s += f'{v}->{self.__grand.__val};\n'
            s += self.__petit.tree2viz()
            s += self.__grand.tree2viz()
        elif self.__petit and not self.__grand:
            s += f'{self.__petit.__val}[style=filled,fillcolor=cadetblue3];\n'
            s += f'{v}->{self.__petit.__val};\n'
            s += f'nullp{v}[shape=point];\n{v}->nullp{v};\n'
            s += self.__petit.tree2viz()
        elif self.__grand and not self.__petit:
            s += f'{self.__grand.__val}[style=filled,fillcolor=darkorange];\n'
            s += f'nullg{v}[shape=point];\n{v}->nullg{v};\n'
            s += f'{v}->{self.__grand.__val};\n'
            s += self.__grand.tree2viz()
        else:
            s += f'nullg{v}[shape=point];\n{v}->nullg{v};\n'
            s += f'nullp{v}[shape=point];\n{v}->nullp{v};\n'
        return s
```


On crée  ensuite le  fichier final au  format `.dot` et  on l'exporte  au format
`.png`.  On  utilise la  bibliothèque  `subprocess`  qui  permet de  lancer  des
commandes `bash` depuis `python`:


```python
    def affiche(self: 'Noeud[C]') -> None:
        """
        Finit de mettre au format dot de graphviz :
        digraph G{
           s1 -> s2;
           s1 -> s3;
        }
        puis exporte au format png et affiche le résultat
        avec l'outil de visualisation par défaut
        """
        nl = '\n'
        s = f'digraph G{{{nl} graph [ordering="out"];{nl}{self.tree2viz()}}}'
        with open('arbre.dot', 'w') as f:
            f.write(s)
        c = 'dot -Tpng arbre.dot -o arbre.png && xdg-open arbre.png'
        subprocess.call(c, shell=True)
```

Pour  avoir  une  sortie  (just  for  fun)  en  mode  texte,  on  peut  utiliser
`graph-easy`:

```python
    def __repr__(self: 'Noeud[C]') -> str:
        """
        Utilise graph-easy pour afficher les arbres en mode texte.
        Au besoin, installer graph-easy:
          $ sudo apt install cpanminus
          $ sudo cpanm Graph::Easy
        Ensuite on récupère la sortie standard qui est au format binaire
        et on l'encode en utf8 pour l'avoir au format str exigé par repr
        """
        nl = '\n'
        s = f'digraph G{{{nl}graph [ordering="out"];{nl}{self.tree2viz()}}}'
        with open('arbre.dot', 'w') as f:
            f.write(s)
        cc = 'graph-easy --from=dot --as_ascii arbre.dot'.split()
        res = subprocess.run(cc, stdout=subprocess.PIPE)
        return res.stdout.decode('utf-8')
```

## Exemple de problèmes 


Vous pouvez explorer les problèmes suivants:

1. Supprimer un noeud.
2. Construire un ABR à partir de deux ABR.
3. Un ABΡest-il unique pour une liste de valeurs données?
4. Avantages/inconvénients par rapport à une table de hachage?
5. Complexité d'un tri à partir d'un ABR (construction + obtention du tri)?
6. Construire tous les ABR pour les entiers de 1 à n.
7. Équilibrer un ABR (même niveau partout sauf éventuellement le dernier).
8. Trouver le plus petit ancêtre commun.
9. Trouver la médiane d'un ABR en temps linéaire.
...



## Arbre caché


Écrire une fonction d'entête `def somme(M)`
qui prend en paramètre une séquence imbriquée, de profondeur et de structure quelconques, dont tous les
composants  élémentaires sont  des  nombres, et  calcule la  somme  de tous  ces
éléments.

Par exemple :

```console
somme([[[1, 2], [3, 4, 5]], 6, [7, 8], 9]) -> 45
```

L'expression booléenne 
```python
isinstance(x, numbers.Real)
```
permet de tester si `x` est un scalaire numérique. Par exemple :

```python
isinstance(1, numbers.Real) -> True
isinstance(2.3e4, numbers.Real) -> True
isinstance([1, 2, 3], numbers.Real) -> False
```
