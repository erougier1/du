% Architecture de Von Neumann

### En Bref:
on découvre sommairement l'architecture commune de toute machine.

## Architecture de Von Neumann

En  1945, John  Von  Neumann  présenta dans  un  article  une organisation  d'un ordinateur qui
continue peu ou prou à servir de modèle actuellement:

![Von Neumann](./IMG/VonN.png)

On distingue:

* le CPU (*Central Processing Unit* ou unité centrale de traitement) est plus communément appelée le *processeur* ;
* La mémoire où sont stockés les données et les programmes ;
* Des *Bus* qui  sont en fait des fils conduisant  des impulsions électriques et qui relient les différents composants :
* Des Entrées-Sorties (E/S ou I/O *Input and Output*) pour échanger avec l'extérieur.

Un programme  est enregistré dans  la mémoire.  L'adresse (un nombre  entier) de
l'instruction en  cours de traitement  est stockée  dans une mémoire  interne au
processeur nommée  *registre compteur de  programme* (cp  ou pc en  Anglais). La
valeur de cette instruction (aussi un entier) est stockée dans une autre mémoire
interne: le *registre  d'instruction* (ri). Le CPU dispose aussi  d'une série de
mémoires internes dans  le *banc de registres* dans lesquelles  sont placées les
données du programme avant utilisation.
On  y trouve  l'UAL (*Unité  Arithmétique  et Logique*  ou ALU  en Anglais)  qui
effectue les opérations arithmétiques et logiques en interprétant les impulsions électriques sortant de
ses *circuits combinatoires*  fabriqués à l'aide de  circuits élémentaires (NAND
en particulier, cf fiche 5).

## Horloge

Le   CPU  dispose   d'une  horloge   qui  va   cadencer  l'accomplissement   des
instructions. L'unité est appelée *cycle*.

Lorsqu'on parle dans le commerce d'un
processeur  cadencé à  3 GHz,  cela signifie  qu'il y  a 3  milliards de  cycles
d'horloge par  seconde. Jusqu'en  2004 environ, la  fréquence des  processeurs a
augmenté  linéairement.  Depuis,  elle  stagne: en  effet,  au-delà  la  chaleur
produite devient trop  importante et pourrait perturber la  lecture des tensions
lues  aux  bornes  des  circuits  de l'UAL  voire  détériorer  physiquement  ces
circuits.  On  cherche   depuis  à  optimiser  autrement   les  processeurs  (cf
organisation de la mémoire).


Dans une  machine de Turing  (cf Fiche 29),  un cycle représente  l'ensemble des
quatre actions: Lire,  Écrire, Changer d'état, se Déplacer (voir  une machine de Turing cadencée à 0,5Hz en action: https://youtu.be/VpDMAVKWvb4). 


Dans un "vrai" processeur, chacune des  cinq actions suivantes est exécutée lors
d'un cycle: Lire l'instruction, Décoder
l'instruction, Exécuter l'opération dans l'UAL,  accéder à la Mémoire en lecture
ou en écriture, Écrire le résultat dans les registres.

Pour gagner  du temps, le processeur  n'exécute pas les instructions  de manière
séquentielle mais  exécute simultanément   plusieurs instructions qui  sont à
des étapes différentes de leur traitement. C'est le principe du *Pipeline d'instructions*:


![Pipeline](./IMG/pipeline.png)

Dans l'exemple  de la  figure ci-dessus,  on a  cinq instructions  qui devraient demander 25 cycles  pour être exécutées. Il en  faut en fait 9 comme  on peut le voir.

Cette  optimisation peut  être  freinée  par des  branchements  (cf section  Jeu d'instructions) ou accélérée par une meilleure organisation de la mémoire.
