import numpy as np
from typing import List, Callable, Dict, Tuple
from matplotlib import pyplot as plt
from matplotlib import animation
from math import factorial

Pixel = float
Ligne = List[Pixel]
Image = List[Ligne]
Image_fractionnee = List[Image]
Tri = Callable[[Image], Image]

#valeurs possibles de sort : "bulles", "insertion", "bogo", "selection"

def visualisation_tri(n: int,
                      sort: str,
                      cmap: str = "inferno",
                      save: bool = False,
                      vitesse: int = 10) -> None:
    index = 0
    liste_sens_index_cocktail : List[Tuple] = [(1, 0, 0, n-2)]*n
    fusion_list : List = []
    if save:
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=60, metadata=dict(artist='Me'), bitrate=1800)

    fig = plt.figure(figsize = (4.5,4.5))
    plt.yticks([])
    plt.xticks([])
    plt.title(f'{sort} sort', y = 1.02, fontsize = 12)
    a = np.random.random((n,n))
    im = plt.imshow(a, cmap = cmap, interpolation='nearest')

    def random_np_array():
        return np.array([np.random.choice([j/(n-1) for j in range(n)], n) for i in range(n)])

    def tri_a_bulles_inverse(mat : Image) -> Image:
        """
        On fait un échange à la fois pour la visualisation
        """
        for ligne in mat:
            for y in range(len(ligne) - 1):
                if ligne[y] > ligne[y + 1]:
                    ligne[y], ligne[y + 1] = ligne[y + 1], ligne[y]
                    break # on sort du for y dès qu'un échange est fait
        return mat

    def tri_insertion(mat : Image) -> Image:
        # Il faut faire avancer l'index à chaque appel interrompu
        # on fait de l'image par image
        nonlocal index
        # index est une variable définie un niveau au-dessus dans visualisation_tri
        # ce n'est donc pas une variable locale
        for ligne in mat:
            j = index # là où on en est
            # j descend avec le -= 1 et monte avec l'index 
            while 0 < j < len(ligne):
                if ligne[j-1] > ligne[j]:
                    ligne[j - 1], ligne[j] = ligne[j], ligne[j - 1]
                else:
                    break
                j -= 1
        index += 1 # une colonne de faite
        return mat
    
    def est_trie(xs : Ligne) -> bool:
        q = 0
        while q < len(xs) - 1 and xs[q] <= xs[q + 1]:
            q += 1
        return q == len(xs) - 1

    def tri_bogo(mat : Image) -> Image:
        # on mélange les lignes tant qu'elles ne sont pas triées...
        for ligne in mat:
            if not est_trie(ligne):
                np.random.shuffle(ligne)
        return mat


    def tri_selection(mat : Image) -> Image:
        # comme pour l'insertion, on avance colonne par colonne
        # cette fois on met chaque élément à sa place finale 
        nonlocal index
        for ligne in mat:
            min_idx = index
            for y in range(index + 1, len(ligne)):
                if ligne[y] < ligne[min_idx]:
                    min_idx = y
            # on place le plus petit élément de la sous-liste au début de celle-ci 
            ligne[index], ligne[min_idx] = ligne[min_idx], ligne[index]
        if index < len(mat)-1:
            index += 1
        return mat

    def tri_cocktail_etape(mat : Image) -> Image:
        # C'est un tri à bulles mais qui change de sens à chaque fois qu'un
        # élément est mis à sa place
        nonlocal liste_sens_index_cocktail
        for index_ligne in range(len(mat)):
            sens, last_index, index_min, index_max = liste_sens_index_cocktail[index_ligne]
            while (last_index<index_max and sens==1) or (last_index>index_min and sens==-1):
                if mat[index_ligne][last_index] > mat[index_ligne][last_index+1]:
                    mat[index_ligne][last_index], mat[index_ligne][last_index+1] = mat[index_ligne][last_index+1], mat[index_ligne][last_index]
                last_index += sens
            if sens==1:
                index_max -= 1
            else:
                index_min += 1
            sens = -sens
            liste_sens_index_cocktail[index_ligne] = (sens, last_index, index_min, index_max)
        return mat

    def fusion(t1 : Ligne,t2 : Ligne) -> Ligne :
        # Fusionne deux listes triées en une seule liste triée de longueur len(t1)+len(t2)
        if t1==[]:
            return t2
        elif t2==[]:
            return t1
        elif t1[0]<t2[0]:
            return [t1[0]]+fusion(t1[1:],t2)
        else:
            return [t2[0]]+fusion(t1,t2[1:])


    def scinde(mat : Image) -> Image_fractionnee:
        # Est appellée une fois au début si le tri est un tri par fusion
        # Renvoie la matrice fractionnée (chaque valeur d'une ligne est 
        # dans une liste de longueur 1, la ligne est donc maintenant une
        # liste de listes)
        new_mat = []
        for ligne in mat:
            new_ligne = []
            for x in ligne:
                new_ligne.append([x])
            new_mat.append(new_ligne)
        return new_mat

    def tri_fusion_etape(mat:Image) -> Image:
        # /!\ /!\ /!\ ATTENTION, NE PEUT ÊTRE APPELLÉ QU'AVEC UNE VALEUR INFÉRIEURE OU ÉGALE À 512 /!\ /!\ /!\ /!\ /!\ /!\ /!\/!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\ /!\  
        # La première fois qu'elle est appellée : n'effectue que la fonction 
        # scinde et renvoie la matrice telle quelle
        # Les fois suivantes : fusionne les listes dans chaque lignes deux par
        #  deux jusqu'à avoir la liste finale triée
        #
        # La liste est enregistrée, et la matrice donnée est ignorée à partie du
        # deuxième appel de la fonction, et la liste est transformée en enlevant
        # les listes dans les lignes pour que le format soit une Image et non une
        # Image fractionnée

        nonlocal fusion_list
        if fusion_list == []:
            fusion_list = scinde(mat)
            new_matrice = mat
        else:
            new_fusion_list = []
            for ligne in fusion_list:
                if len(ligne)%2==1:
                    ligne.append([])
                new = []
                for x in range(len(ligne)//2):
                    new.append(fusion(ligne[2*x], ligne[(2*x)+1]))
                new_fusion_list.append(new)
            fusion_list = new_fusion_list
            new_matrice=[]
            for ligne in fusion_list:
                new_line = []
                for sequence in ligne:
                    for x in sequence:
                        new_line.append(x)
                new_matrice.append(new_line)
        return new_matrice


    switch: Dict[str, Tri] = {
        "bulles" : tri_a_bulles_inverse,
        "insertion" : tri_insertion,
        "bogo" : tri_bogo,
        "selection" : tri_selection,
        "cocktail" : tri_cocktail_etape,
        "fusion" : tri_fusion_etape
    }

    def init() :
        """
        initialise l'image avec les lignes aléatoires
        """
        im.set_data(random_np_array())
        return [im]


    def animate(i: int):
        """
        On déclare à l'avance combien d'images seront crées
        C'est le paramèter de l'animation
        """
        arr = im.get_array() # image matplotlib -> array
        a_liste = arr.tolist() # array -> list
        a_liste_triee_etape = switch[sort](a_liste) # on effectue une étape du tri 
        arr_trie_etape = np.array(a_liste_triee_etape) # liste -> array
        im.set_array(arr_trie_etape) # array -> matplotlib image
        return [im]

    switch_frames: Dict[str, int] = {
        "bulles" : (n**2)//3, # nb maxi d'échanges : complexité au pire
        "insertion" : 2*n, # 
        "bogo" : factorial(n*n),
        "selection" : n**n,
        "cocktail" : n*4,
        "fusion" : n**2
    }
    
    frames: int = switch_frames[sort]

    anim = animation.FuncAnimation(fig, animate, init_func=init,
                            frames=frames, interval=vitesse, blit=True)
    if save:
        anim.save(f'{sort}_{n}_{n}_{cmap}.mp4', writer = writer)

    plt.show()


if __name__ == '__main__':
    tri = input('Tri ?  "bulles", "insertion", "bogo", "selection", "cocktail", "fusion" : \n')
    n = int(input('Combien de colonnes ? \n'))
    vitesse = int(input("intervalle de temsp entre 2 images en ms :\n"))
    save = eval(input("Enregistrer l'animation ? True/False\n"))
    visualisation_tri(n, tri, save = save, vitesse=vitesse)
