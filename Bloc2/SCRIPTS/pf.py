from typing import TypeVar, Sequence, Optional, Tuple

T = TypeVar('T')


def compte(x: T, xs: Sequence[T], acc: int = 0) -> int:
    if len(xs) == 0:
        return acc
    else:
        return compte(x, xs[1:], acc + (1 if x == xs[0] else 0))


print(compte("a", "abracadabra"))
print(compte(1, (1,0,1,1,0,1,0,1,1)))
print(compte(1, range(10)))
print(compte(1, [i for i in range(10)]))



def existe_majoritaire(xs: Sequence[T]) -> str:
    for el in xs:
        c = compte(el, xs)
        if c > len(xs) // 2:
            return f"{el} est majoritaire"
        else:
            continue
    return f"Pas d'élément majoritaire"

print(existe_majoritaire("abracadabra"))


def majoritaire(xs: Sequence[T]) -> Tuple[Optional[T], int]:
    n = len(xs)
    gauche, droite = xs[:n//2], xs[n//2:]
    if n == 1:
        return (xs[0], 1)
    eg, cg = majoritaire(gauche)
    ed, cd = majoritaire(droite)
    if cg > 0:
        cg += compte(eg, droite)
    if cd > 0:
        cd += compte(ed, gauche)
    if cg > n//2:
        return (eg, cg)
    if cd > n//2:
        return (ed, cd)
    return (None, 0)
    
print(majoritaire("abracadabraaa"))