from random import choice

def rapide(xs: list) -> list:
    if len(xs) <= 1:
        return xs
    pivot = choice(xs)
    petit = [x for x in xs if x < pivot]
    egal = [x for x in xs if x == pivot]
    grand = [x for x in xs if x > pivot]
    return rapide(petit) + egal + rapide(grand)

print(rapide([2,4,2,3,6,8,1,5,2,7,6,4,9,8,2,3]))