def fusion_en_place(xs, tampon, lo, hi):
    mid = (lo + hi) // 2
    i, j = lo, mid # le tableau gauche s'arrête au milieu et celui de droite commence au milieu
    for pos in range(lo, hi):
        if j >= hi: # on a rangé toute la partie droite
            tampon[pos] = xs[i] # on met le prochain élément de la partie gauche restante
            i += 1 # on avance dans la partie gauche d'un cran
        elif i >= mid: # on a rangé toute la partie gauche
            tampon[pos] = xs[j] # on met met le prochain élément de la partie droite restante
            j += 1  # on avance dans la partie droite d'un cran
        elif xs[i] <= xs[j]: # il en reste des 2 côtés : qui est le plus petit ?
            tampon[pos] = xs[i] # on place le plus petit
            i += 1 # on avance dans la partie gauche d'un cran
        else: 
            tampon[pos] = xs[j]
            j += 1 # on a placé xs[j] donc on avance à droite
    for pos in range(lo, hi):
        xs[pos] = tampon[pos] # xs trié entre les indices lo et hi

def tri_fusion_en_place(xs):
    def aux(tampon, lo, hi):
        if hi - lo >= 2:
            mid = (lo + hi) // 2
            aux(tampon, lo, mid)
            aux(tampon, mid, hi)
            fusion_en_place(xs, tampon, lo, hi)
    aux([0]*len(xs), 0, len(xs))

xs = [1,5,2,4,9,3,2,5,47,56,12,23,65,54]
print(xs)
tri_fusion_en_place(xs)
print(xs)


        


