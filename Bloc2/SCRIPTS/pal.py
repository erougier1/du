def est_palindrome(mot: str) -> bool:
    if len(mot) <= 1:
        return True
    print(mot)
    if mot[0] != mot[-1]:
        return False
    else:
        return est_palindrome(mot[1:-1])

print(est_palindrome("abcdeffdcba"))