class Recursif(Exception):
    """
    Une classe d'exception qui est levée quand on s'aperçoit que la fonction est
    récursive. On récupère alors les arguments
    """
    def __init__(self, *args, **kwargs):
        self.args = args # les arguments simples (dans un tuple)
        self.kwargs = kwargs # les arguments nommés par défaut (dans un dictionnaire)

def recursif(*args, **kwargs):
    """
    Crée une instance de de la classe précédente
    """
    raise Recursif(*args, **kwargs)
        
def rec_term(f):
    def derecursifie(*args, **kwargs):
        while True:
            try:
                return f(*args, **kwargs)
            except Recursif as r:
                # si appel récursif, on échange les arguments mis à jour
                args = r.args 
                kwargs = r.kwargs
                continue
    return derecursifie


@rec_term
def fact(n: int, acc:int = 1) -> int:
    return acc if n == 0 else recursif(n - 1, n*acc)


