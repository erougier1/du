from functools import reduce

class Perm :
    """ Groupe des permutations, la loi étant la composition """

    def __init__(self, *args) :
        """ Permutation définie par la liste des images de [0,1,2,...] """
        self.images = list(args)

    def __repr__(self) :
        return (' ').join( [str(x) for x in self.images] )

    def __len__(self):
        return len(self.images)

    def __neg__(self) :
        """ permutation réciproque de self """
        im = self.images
        n = len(im)
        return [couple[1] for couple in sorted( zip(im, range(n)) )]

    def __add__(self, other) :
        """ fabrique la composée self o other """ 
        assert len(self)==len(other), 'Les permutations doivent être de meme taille'
        return Perm(*[self.images[other.images[i]] for i in range(len(self))])
    

    def permute(self,xs) :
        """ si xs = [xs[0], xs[1], xs[2],...], p.permute(xs) = [xs[p(0)], xs[p(1)], xs[p(2)],...] """
        return [xs[i] for i in self.images]

    def circulaire(self, rangs) :
        """ permutation circulaire de rangs rangs """
        im = self.images
        n = len(im)
        return 0

###
#
#  Les chiffrements par blocs : ECB, CBC
#
###

def decoupe_en_blocs(nb_bits_par_bloc, les_bits) :
    n = nb_bits_par_bloc
    reste = len(les_bits) % n
    bs = les_bits + [0]*(0 if reste == 0 else n - reste)
    return [ bs[i*n:(i+1)*n] for i in range(len(bs)//n) ]

 

def ecb(cle, liste_bits) :
    """renvoie la liste des permutations des blocs construits à partir d'une liste de bits"""
    return [cle.permute(bloc) for bloc in decoupe_en_blocs(len(cle), liste_bits)]

def string2bin(s, n) :
    """ transforme un caractère en sa représentation sur n bits"""
    return 0

def bin2string(bs) :
    """ transforme une liste de bits en l'entier décimal correspondant"""
    return 0

def cats(xs) :
    """concatène une liste de listes ou de chaînes"""
    return 0

def code_bloc_string(code_bloc, cle, n_bits, chaine) :
    """applique un chiffrement par bloc sur une chaîne en donnant le nom du
       chiffrement, sa clé et le nb de bits sur lequel les caractères sont codés """
    return 0


# In [29]: phrase = "Maman les petits bateaux qui vont sur l'eau ont-ils des jambes ?"

# In [30]: c = code_bloc_string(ecb, Perm([2,0,3,1,5,4,6]), 7, phrase)

# In [32]: code_bloc_string(ecb, -Perm([2,0,3,1,5,4,6]), 7, c)
# Out[32]: "Maman les petits bateaux qui vont sur l'eau ont-ils des jambes ?"

# In [33]: print(c)
# 3);):+h+j9j,)j+)kikn?>mk:+)?>j9:*+<);,+_

# In [34]: c = code_bloc_string(ecb, Perm([2,0,3,1,5,4,6]), 7, 'maman')

# In [35]: print(c)
# ;);)>

# In [37]: code_bloc_string(ecb, -Perm([2,0,3,1,5,4,6]), 7, ';);)>')
# Out[37]: 'maman'


#### MAIN

p = Perm(1,3,4,0,2)
q = Perm(2,4,1,3,0)
xs = [0,1,2,3,4]
print(p.permute(xs))