# -*- coding: utf-8 -*-

import glob
# The glob module finds all the pathnames matching a specified pattern according to the rules used by the Unix shell
from pathlib import Path
# The final path component, without its suffix


mon_chemin = input('Quel est le chemin relatif du répertoire contenant les fichiers csv ?\n')
#"./MIB_Files/"

mon_alias = input('Alias du fichier py créé (sera ./MaBase_alias.py) ?\n')

mon_fic = "MaBase_%s.py" % mon_alias

mes_csv_nom = [Path(f).stem for f in glob.glob(mon_chemin + "*.csv")]

mon_py = open(mon_fic,"w+")


def depuis_csv(nom_fichier_csv: str):
    """
    Crée une liste de dictionnaires, un par ligne.
    La 1ère ligne du fichier csv est considérée comme la ligne des noms des champs
    """
    lecteur = csv.DictReader(open(nom_fichier_csv,'r')) 
    return [dict(ligne) for ligne in lecteur]



def creer_bases() -> None:
    mon_py.write('#'*30 +'\n##  Les bases de travail du répertoire ' + mon_chemin + '\n' + '#'*30 + '\n\n\n')
    for nom in mes_csv_nom:
        mon_py.write(nom + " =  ")
        mon_py.write(str(depuis_csv(mon_chemin + nom + '.csv')))
        mon_py.write("\n\n")
        

def ferme() -> None:
    mon_py.close()

creer_bases()
ferme()

#from MaBase_MIB import *


