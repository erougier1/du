brut = open('texte.txt')
zero_un = brut.read()

def octet2int(chaine) :
    code = 0
    for bit in chaine :
        code = (code << 1) + int(bit)
    return code
    # return reduce(lambda code, bit : (code << 1) + int(bit), chaine, 0 )

def decode(message) :
    chaine = ''
    taille = len(message) >> 3
    for no_octet in range(taille) :
        chaine += chr(octet2int(message[(no_octet << 3) : (no_octet+1) << 3]))
    print(chaine)

def decode2(message) :
    chaine = ''
    taille = len(message) >> 3
    no_octet = 0
    while no_octet < taille :
        if message[8*no_octet] == '0' :
            chaine += chr(octet2int(message[8*no_octet:8*(no_octet+1)]))
            no_octet += 1
        elif message[8*no_octet:8*no_octet + 3] == '110' :
            chaine += chr(octet2int(message[8*no_octet + 3:8*(no_octet+1)] +
                                    message[8*(no_octet+1) + 2:8*(no_octet+2)]))
            no_octet += 2
        elif message[8*no_octet:8*no_octet + 4] == '1110' :
            chaine += chr(octet2int(message[8*no_octet + 4:8*(no_octet+1)] +
                                    message[8*(no_octet+1) + 2:8*(no_octet+2)] +
                                    message[8*(no_octet+2) + 2:8*(no_octet+3)]))
            no_octet += 3
        else :
            chaine += chr(octet2int(message[8*no_octet + 5:8*(no_octet+1)] +
                                    message[8*(no_octet+1) + 2:8*(no_octet+2)] +
                                    message[8*(no_octet+2) + 2:8*(no_octet+3)] +
                                    message[8*(no_octet+3) + 2:8*(no_octet+4)] ))
            no_octet += 4
    print(chaine)


def int2octet(n) :
    bin = ''
    q = n
    while q > 0 :
        bin += str(q % 2)
        q //= 2
    return bin[-1::-1]


def utf(lettre) :
    code = ord(lettre)
    ch = int2octet(code)
    taille = len(ch)
    if code <= 0x7f :
        return ''.join(['0' for _ in range(8 - taille)]) + ch
    if code <= 0x7ff :
        rep = ''.join(['0' for _ in range(11 - taille)]) + ch
        return '110' + rep[:5] + '10' + rep[5:]
    if code <= 0xffff :
        rep = ''.join(['0' for _ in range(16 - taille)]) + ch
        return '1110' + rep[:4] + '10' + rep[4:10] + '10' + rep[10:]
    rep = ''.join(['0' for _ in range(21 - taille)]) + ch
    return '11110' + rep[:3] + '10' + rep[3:9] + '10' + rep[9:15] + '10' + rep[15:]

def utf_message(message) :
    chaine = ''
    for lettre in message :
        chaine += utf(lettre)
    return chaine
