import numpy as np
import numpy.linalg as alg

def J(n) :
    return np.matrix([[1 if j == (n - 1) - i else 0 for j in range(n)] for i in range(n)])

def randMatrix(n, p) :
    return np.matrix([[np.random.randint(100) for i in range(p)] for i in range(n)])

def centro(A) :
    n,p = A.shape
    assert n == p, "La matrice doit être carrée"
    Jn = J(n)
    return Jn * A * Jn


def est_sym(a) :
    return np.allclose(a, a.transpose())

def est_anti_sym(a) :
    return np.allclose(a, - a.transpose())

def est_cent_sym(a) :
    return np.allclose(a, centro(a))

def est_cent_anti(a) :
    return np.allclose(a, -centro(a))


def decompo(A) :
    tA = A.transpose()
    sA = (A + tA) / 2
    aA = (A - tA) / 2
    d = [(sA + centro(sA)) / 2, (sA - centro(sA))/2, (aA + centro(aA)) / 2, (aA - centro(aA))/2]
    assert est_sym(d[0]) and est_cent_sym(d[0]), "première composante :-("
    assert est_sym(d[1]) and est_cent_anti(d[1]), "seconde composante :-("
    assert est_anti_sym(d[2]) and est_cent_sym(d[2]), "troisième composante :-("
    assert est_anti_sym(d[3]) and est_cent_anti(d[3]), "quatrième composante :-("
    assert np.allclose(sum(d), A), "somme :-("
    return d
